package com.ikryv.cityroutes.proxy;

import java.util.List;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.ikryv.cityroutes.domain.*;

@FeignClient(value="city-routes-repository")
@RibbonClient(name="city-routes-repository")
public interface CityRoutesRepositoryProxy {
    
    @GetMapping(value="/routes")
    List<Route> findAllRoutes();
    
    @GetMapping(value="/cities")
    List<City> findAllCities();

}
