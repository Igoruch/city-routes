package com.ikryv.cityroutes.service;

import static java.util.stream.Collectors.joining;

import java.util.*;
import java.util.Map.Entry;

import com.ikryv.cityroutes.domain.Node;

public class FindRouteService {
    private static final String JOIN_SYMBOL = " -> ";

    public static String calculateShortestPath(Map<Integer, Node> nodeIdToNodeMap, Node startNode, Node endNode) {
        startNode.setDistance(0);

        Set<Node> settledNodes = new HashSet<>();
        Set<Node> unsettledNodes = new HashSet<>();

        unsettledNodes.add(startNode);

        Node currentNode = null;

        while (!unsettledNodes.isEmpty()) {
            currentNode = getLowestDistanceNode(unsettledNodes);
            unsettledNodes.remove(currentNode);
            for (Entry<Node, Integer> adjacencyPair : currentNode.getAdjacentNodes().entrySet()) {
                Node adjacentNode = nodeIdToNodeMap.get(adjacencyPair.getKey().getId());
                int edgeWeight = adjacencyPair.getValue();
                if (!settledNodes.contains(adjacentNode)) {
                    calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                    unsettledNodes.add(adjacentNode);
                }
            }
            settledNodes.add(currentNode);
            if (currentNode.equals(endNode)) {
                break;
            }
        }

        if (!currentNode.equals(endNode) || currentNode.getShortestPath().isEmpty()) {
            return String.format("There is no route from city %s to city %s!", startNode.getName(), endNode.getName());
        }

        return String.join(JOIN_SYMBOL,
                currentNode.getShortestPath().stream()
                        .map(Node::getName)
                        .collect(joining(JOIN_SYMBOL)),
                currentNode.getName());
    }

    private static Node getLowestDistanceNode(Set<Node> unsettledNodes) {
        Node lowestDistanceNode = null;
        int lowestDistance = Integer.MAX_VALUE;
        for (Node node : unsettledNodes) {
            int nodeDistance = node.getDistance();
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                lowestDistanceNode = node;
            }
        }
        return lowestDistanceNode;
    }

    private static void calculateMinimumDistance(Node evaluationNode,
            int edgeWeigh, Node sourceNode) {
        Integer sourceDistance = sourceNode.getDistance();
        if (sourceDistance + edgeWeigh < evaluationNode.getDistance()) {
            evaluationNode.setDistance(sourceDistance + edgeWeigh);
            LinkedList<Node> shortestPath = new LinkedList<>(sourceNode.getShortestPath());
            shortestPath.add(sourceNode);
            evaluationNode.setShortestPath(shortestPath);
        }
    }
}
