package com.ikryv.cityroutes.utils;

import static java.util.stream.Collectors.toMap;

import java.util.*;
import java.util.function.Predicate;

import com.ikryv.cityroutes.domain.*;

public class NodeUtils {
    private NodeUtils() {
    };

    public static Map<Integer, Node> getNodeIdToNodeMap(List<City> cityList, List<Route> routeList) {

        return cityList.stream()
                .collect(toMap(City::getId, city -> createNode(city, routeList)));
    }

    private static Node createNode(City city, List<Route> routeList) {
        Predicate<Route> containsAdjacentNode = route -> route.getStartCity().equals(city);
        Node node = new Node(city.getId(), city.getName());

        routeList.stream()
                .filter(containsAdjacentNode)
                .forEach(route -> {
                    City adjacentCity = route.getEndCity();
                    node.getAdjacentNodes().put(new Node(adjacentCity.getId(), adjacentCity.getName()),
                            route.getWeight());
                });

        return node;
    }

}
