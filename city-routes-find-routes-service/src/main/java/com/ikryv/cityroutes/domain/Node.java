package com.ikryv.cityroutes.domain;

import java.util.*;

public class Node {
    
    private int id;
    
    private String name;

    private List<Node> shortestPath = new LinkedList<>();

    private int distance = Integer.MAX_VALUE;

    Map<Node, Integer> adjacentNodes = new HashMap<>();
    
    public Node(int id, String name) {
        this.id = id;
        this.name = name;
    }
    

    public void addDestination(Node destination, int distance) {
        adjacentNodes.put(destination, distance);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Node> getShortestPath() {
        return shortestPath;
    }

    public void setShortestPath(List<Node> shortestPath) {
        this.shortestPath = shortestPath;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public Map<Node, Integer> getAdjacentNodes() {
        return adjacentNodes;
    }

    public void setAdjacentNodes(Map<Node, Integer> adjacentNodes) {
        this.adjacentNodes = adjacentNodes;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Node other = (Node) obj;
        return Objects.equals(name, other.name) && id == other.id;
    }

    @Override
    public String toString() {
        return "Node [id=" + id + ", name=" + name + ", shortestPath=" + shortestPath + ", distance=" + distance
                + ", adjacentNodes=" + adjacentNodes + "]";
    }

}
