package com.ikryv.cityroutes.domain;

import java.util.Objects;

public class Route {

    private int id;
    private City startCity;
    private City endCity;
    private int weight;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public City getStartCity() {
        return startCity;
    }

    public void setStartCity(City startCity) {
        this.startCity = startCity;
    }

    public City getEndCity() {
        return endCity;
    }

    public void setEndCity(City endCity) {
        this.endCity = endCity;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public int hashCode() {
        return Objects.hash(endCity, id, startCity, weight);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Route other = (Route) obj;
        return Objects.equals(endCity, other.endCity) && id == other.id && Objects.equals(startCity, other.startCity)
                && weight == other.weight;
    }

    @Override
    public String toString() {
        return "Route [id=" + id + ", startCity=" + startCity + ", endCity=" + endCity + ", weight=" + weight + "]";
    }
}
