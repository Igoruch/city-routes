package com.ikryv.cityroutes.webservice;

import static com.ikryv.cityroutes.service.FindRouteService.calculateShortestPath;
import static com.ikryv.cityroutes.utils.NodeUtils.getNodeIdToNodeMap;

import java.util.Map;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import com.ikryv.cityroutes.domain.Node;
import com.ikryv.cityroutes.proxy.CityRoutesRepositoryProxy;

@RestController
@RequestMapping(value = "/findRoutes")
public class FindRoutesController {

    private static Logger log = LoggerFactory.getLogger(FindRoutesController.class);

    @Autowired
    private CityRoutesRepositoryProxy proxy;

    @Autowired
    private Environment environment;

    @GetMapping
    public String findRoutes(@RequestParam(name = "startCityId", required = true) int startCityId,
            @RequestParam(name = "endCityId", required = true) int endCityId) {
        
        log.info("Start calculating route on port {}", environment.getProperty("local.server.port"));

        Map<Integer, Node> nodeIdToNodeMap = getNodeIdToNodeMap(proxy.findAllCities(), proxy.findAllRoutes());

        Node startNode = nodeIdToNodeMap.get(startCityId);
        Node endNode = nodeIdToNodeMap.get(endCityId);

        String route = calculateShortestPath(nodeIdToNodeMap, startNode, endNode);

        log.info("Route calculation finished successfully");

        return route;
    }

}
