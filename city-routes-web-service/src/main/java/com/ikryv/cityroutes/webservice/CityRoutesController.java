package com.ikryv.cityroutes.webservice;

import static java.util.stream.Collectors.toMap;

import java.util.*;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.ikryv.cityroutes.domain.City;
import com.ikryv.cityroutes.service.CityRoutesService;

@Controller
@RequestMapping({ "/", "/city-routes" })
public class CityRoutesController {

    private static Logger log = LoggerFactory.getLogger(CityRoutesController.class);
    private Map<Integer, String> cityIdToNameMap;

    @Autowired
    private CityRoutesService service;

    @GetMapping()
    public String getCityRoutesPage(Model model) {
        List<City> cityList = service.findAllCities();
        cityIdToNameMap = cityList.stream()
                .collect(toMap(City::getId, City::getName));
        
        model.addAttribute("cityList", cityList);

        return "city-routes";
    }

    @GetMapping(value = "findRoutes")
    public String findRoutes(Model model, @RequestParam("startCityId") int startCityId,
            @RequestParam("endCityId") int endCityId) {

        String route = "";

        if (startCityId == endCityId) {
            route = String.join(" -> ", cityIdToNameMap.get(startCityId), cityIdToNameMap.get(endCityId));
        } else {
            route = service.findRoutes(startCityId, endCityId);
        }

        model.addAttribute("route", route);
        return "route";
    }

}
