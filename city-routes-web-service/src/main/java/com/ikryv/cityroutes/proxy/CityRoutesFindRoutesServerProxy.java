package com.ikryv.cityroutes.proxy;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "city-routes-findroutes-server")
@RibbonClient(name = "city-routes-findroutes-server")
public interface CityRoutesFindRoutesServerProxy {

    @GetMapping(value = "/findRoutes")
    String findRoutes(@RequestParam(name = "startCityId", required = true) int startCityId,
            @RequestParam(name = "endCityId", required = true) int endCityId);
}
