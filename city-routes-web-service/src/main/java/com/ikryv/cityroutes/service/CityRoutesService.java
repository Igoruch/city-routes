package com.ikryv.cityroutes.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ikryv.cityroutes.domain.City;
import com.ikryv.cityroutes.proxy.*;

@Service
public class CityRoutesService {
    
    @Autowired
    private CityRoutesRepositoryProxy repositoryProxy;
    
    @Autowired
    private CityRoutesFindRoutesServerProxy findRoutesProxy;
    
    public List<City> findAllCities(){
        return repositoryProxy.findAllCities();
    }

    public String findRoutes(int startCityId, int endCityId){
        return findRoutesProxy.findRoutes(startCityId, endCityId);
    }

}
