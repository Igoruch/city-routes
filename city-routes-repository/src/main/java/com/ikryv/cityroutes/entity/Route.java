package com.ikryv.cityroutes.entity;

import javax.persistence.*;

@Entity
@Table(name = "Route")
public class Route {

    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne
    @JoinColumn(name = "Start_City_Id")
    private City startCity;

    @ManyToOne
    @JoinColumn(name = "End_City_Id")
    private City endCity;

    @Column(name = "Weight")
    private int weight;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public City getStartCity() {
        return startCity;
    }

    public void setStartCity(City startCity) {
        this.startCity = startCity;
    }

    public City getEndCity() {
        return endCity;
    }

    public void setEndCity(City endCity) {
        this.endCity = endCity;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Route [id=" + id + ", startCity=" + startCity + ", endCity=" + endCity + ", weight=" + weight + "]";
    }

}
