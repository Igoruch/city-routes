package com.ikryv.cityroutes.entity;

import javax.persistence.*;

@Entity
@Table(name="City")
public class City {
    
    @Id
    @Column(name="Id")
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;
    
    @Column(name="Name")
    private String name;

    public City() {
    }

    public City(int id, String name) {
        this.id = id;
        this.name=name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() {
        return "City [id=" + id + ", name=" + name + "]";
    }

}
