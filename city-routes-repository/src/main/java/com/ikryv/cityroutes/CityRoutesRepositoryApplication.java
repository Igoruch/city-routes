package com.ikryv.cityroutes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CityRoutesRepositoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(CityRoutesRepositoryApplication.class, args);
	}

}
