package com.ikryv.cityroutes.webservice;

import java.util.List;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import com.ikryv.cityroutes.entity.City;
import com.ikryv.cityroutes.repository.CityRepository;

@RestController()
@RequestMapping(value = "/cities")
public class CityController {

    private static Logger log = LoggerFactory.getLogger(CityController.class);

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private Environment environment;

    @GetMapping()
    public List<City> findAllCities() {
        log.info("Instance of city-routes-repository on port: {} start retrieving all cities",
                environment.getProperty("local.server.port"));

        return (List<City>) cityRepository.findAll();
    }

}
