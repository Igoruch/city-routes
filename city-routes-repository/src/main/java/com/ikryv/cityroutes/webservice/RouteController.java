package com.ikryv.cityroutes.webservice;

import java.util.List;

import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import com.ikryv.cityroutes.entity.Route;
import com.ikryv.cityroutes.repository.RouteRepository;

@RestController()
@RequestMapping(value="/routes")
public class RouteController {
    
    private static Logger log = LoggerFactory.getLogger(RouteController.class);
    
    @Autowired
    private RouteRepository routeRepository;
    
    @Autowired
    private Environment environment;
    
    @GetMapping
    public List<Route> findAllRoutes() {
        log.info("Instance of city-routes-repository on port: {} start retrieving all routes",
                environment.getProperty("local.server.port"));
        
        return (List<Route>) routeRepository.findAll();
    }

}
