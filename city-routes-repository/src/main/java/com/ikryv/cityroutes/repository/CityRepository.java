package com.ikryv.cityroutes.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ikryv.cityroutes.entity.City;

@Repository
public interface CityRepository extends CrudRepository<City, Integer>{

}
