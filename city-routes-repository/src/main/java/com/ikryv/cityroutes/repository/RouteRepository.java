package com.ikryv.cityroutes.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ikryv.cityroutes.entity.Route;

@Repository
public interface RouteRepository extends CrudRepository<Route, Integer>{

}
