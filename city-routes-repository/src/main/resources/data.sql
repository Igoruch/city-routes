INSERT INTO City (Name) VALUES 
('A'),
('B'),
('C'),
('D'),
('E'),
('F');

INSERT INTO Route (Start_City_Id, End_City_Id, Weight) VALUES 
(1, 2, 4),
(1, 3, 3),
(1, 4, 7),
(2, 4, 3),
(2, 5, 2),
(3, 5, 3),
(4, 6, 2),
(5, 6, 2);
