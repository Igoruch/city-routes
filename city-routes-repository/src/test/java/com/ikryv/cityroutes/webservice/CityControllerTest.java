package com.ikryv.cityroutes.webservice;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.*;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ikryv.cityroutes.entity.City;
import com.ikryv.cityroutes.repository.CityRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(CityController.class)
public class CityControllerTest {
    
    @MockBean
    private CityRepository cityRepository;
    @Autowired
    private MockMvc mockMvc;
    
    @Test
    public void findAllCities() throws Exception{
        List<City> mockCityList = new ArrayList<>();
        mockCityList.add(new City(1, "A"));
        mockCityList.add(new City(2, "B"));
        
        given(cityRepository.findAll()).willReturn(mockCityList);
        this.mockMvc.perform(get("/cities"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", Matchers.hasSize(2)))
        .andExpect(jsonPath("$[0]['name']", is("A")));
    }

}
