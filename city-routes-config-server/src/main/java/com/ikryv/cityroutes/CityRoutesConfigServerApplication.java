package com.ikryv.cityroutes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class CityRoutesConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CityRoutesConfigServerApplication.class, args);
	}

}
