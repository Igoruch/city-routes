package com.ikryv.cityroutes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class CityRoutesEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CityRoutesEurekaServerApplication.class, args);
	}

}
